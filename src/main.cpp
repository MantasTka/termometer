#include "Arduino.h"
#include "thermometer.h"
#include "stateTemp.h"

#define BUTTON_PRESSED 2

Thermometer *thermometer;

volatile boolean buttonPressed;

void
buttonInterrupt() {
    buttonPressed = true;
}

void
setup() {
    Serial.begin(9600);
    buttonPressed = false;
    thermometer         = new Thermometer();
    delay(2000);
    thermometer->setState(new StateTemp());

    attachInterrupt(
      digitalPinToInterrupt(BUTTON_PRESSED), buttonInterrupt, RISING);
}

void
loop() {
    if(buttonPressed) {
        thermometer->handleInput(buttonPressed);
        buttonPressed = false;
    }
    thermometer->update();
    delay(1000);
}
