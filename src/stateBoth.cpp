#include "stateBoth.h"
#include "LiquidCrystal.h"
#include "EduIntro.h"
#include "stateTemp.h"
#include "stateMoisture.h"
#include "state.h"

void StateBoth::onEnter(LiquidCrystal & lcd){
    lcd.setCursor(0,0);
    lcd.print(StateMoisture::getMsg());
    lcd.setCursor(0,1);
    lcd.print(StateTemp::getMsg());
}

void StateBoth::update(LiquidCrystal & lcd, DHT11 & dht11){
    String moisture = getMoistureMsg(dht11);
    if(moisture != "NAN"){
        lcd.setCursor(moisture.length(),0);
        lcd.print(moisture);
    }
    String temperature = getTemperatureMsg(dht11);
    if(temperature != "NAN"){
        lcd.setCursor(temperature.length(),0);
        lcd.print(temperature);
    }
}

State* StateBoth::handleInput(bool input){
    if(input == true){
        return new StateTemp();
    }
    return nullptr;      
}