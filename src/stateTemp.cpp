#include "StateTemp.h"
#include "LiquidCrystal.h"
#include "EduIntro.h"
#include "utilities.h"
#include "stateMoisture.h"

StateTemp::StateTemp():msg("Oro temp.: "), temperature(0){

}

void StateTemp::onEnter(LiquidCrystal & lcd){
  lcd.setCursor(0,0);
  lcd.print(msg);
}

void StateTemp::update(LiquidCrystal & lcd, DHT11 & dht11){
  String tempMsg = getTemperatureMsg(dht11);
  if(tempMsg != "Nan"){
    lcd.setCursor(msg.length(),0);
    lcd.print(tempMsg);
  }
}

State* StateTemp::handleInput(bool input){
  if(input == true){
    return new StateMoisture();
  }
  return nullptr;
}

const String & StateTemp::getMsg(){
  return msg;
}

String StateTemp::getTemperatureMsg(DHT11 & dht11){
  auto temp = dht11.readCelsius();
  if(temp == temperature){
    return "NAN";
  } 
  temperature = temp;
  String tempMsg = String(temp);
  tempMsg += Util::utf8ascii("°C  ");
  return tempMsg;
}
