#include "thermometer.h"
#include "LiquidCrystal.h"
#include "state.h"
#include "stateHello.h"

void Thermometer::setState(State* state){
    if(state != nullptr){
        thermometerState->onExit(lcd);
        delete thermometerState;
        thermometerState = state;
        thermometerState->onEnter(lcd);
    }
}

void Thermometer::update(){
    dht11.update();
    thermometerState->update(lcd, dht11);
}

void Thermometer::handleInput(bool input){
    auto state=thermometerState->handleInput(input);
    if(state != nullptr){
        thermometerState->onExit(lcd);
        delete thermometerState;
        thermometerState = state;
        thermometerState->onEnter(lcd);
    }
}

Thermometer::Thermometer():thermometerState(new StateHello), lcd(RS,EN,D_PIN4,D_PIN5,D_PIN6,D_PIN7), dht11(DHTPIN){
    lcd.begin(COLS, ROWS);
    lcd.clear();
    thermometerState->onEnter(lcd);
}
    
Thermometer::~Thermometer(){
    thermometerState->onExit(lcd);
    delete thermometerState;
}