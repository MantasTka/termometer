#include "stateMoisture.h"
#include "LiquidCrystal.h"
#include "EduIntro.h"
#include "utilities.h"
#include "stateBoth.h"

StateMoisture::StateMoisture():msg(Util::utf8ascii("Oro drėg.: ")), moisture(0){

}

void StateMoisture::onEnter(LiquidCrystal & lcd){
  lcd.setCursor(0,0);
  lcd.print(msg);
}

void StateMoisture::update(LiquidCrystal & lcd, DHT11 & dht11){
  String moistureMsg = getMoistureMsg(dht11);
  if(moistureMsg != "Nan"){
    lcd.setCursor(msg.length(),0);
    lcd.print(moistureMsg);
  }
}

State* StateMoisture::handleInput(bool input){
  if(input == true){
    return static_cast<StateMoisture*>(new StateBoth());
  }
  return nullptr;
}

const String & StateMoisture::getMsg(){
  return msg;
}

String StateMoisture::getMoistureMsg(DHT11 & dht11){
  auto tempMoisture = dht11.readCelsius();
  if(moisture == tempMoisture){
    return "NAN";
  } 
  moisture = tempMoisture;
  String moistureMsg = String(moisture);
  moistureMsg += Util::utf8ascii("%  ");
  return moistureMsg;
}