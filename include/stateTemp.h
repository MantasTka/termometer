#ifndef PROJECT_STATETEMP_H
#define PROJECT_STATETEMP_H

#include "state.h"
#include "Arduino.h"

class StateTemp:public State{
public:
    StateTemp();
    virtual ~StateTemp()=default;
    void onEnter(LiquidCrystal & lcd) override;
    void update(LiquidCrystal & lcd, DHT11 & dht11) override;
    State* handleInput(bool input) override;
private:
    const String msg;
    int temperature;
protected:
    const String & getMsg();
    String getTemperatureMsg(DHT11 & dht11);
};

#endif