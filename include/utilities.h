//
// https://playground.arduino.cc/Main/Utf8ascii/
//

#ifndef UTILITIES_H
#define UTILITIES_H

#include "Arduino.h"

namespace Util {
// ****** UTF8-Decoder: convert UTF8-string to extended ASCII *******
static byte c1; // Last character buffer

// Convert a single Character from UTF8 to Extended ASCII
// Return "0" if a byte has to be ignored
inline byte
utf8ascii(byte ascii) {
    if(ascii < 128) // Standard ASCII-set 0..0x7F handling
    {
        c1 = 0;
        return (ascii);
    }

    // get previous input
    byte last = c1;    // get last char
    c1        = ascii; // remember actual character

    switch(last) // conversion depending on first UTF8-character
    {
    case 0xC2:
        return (ascii);
    case 0xC3:
        return (ascii | 0xC0);
    case 0x82:
        if(ascii == 0xAC) return (0x80); // special case Euro-symbol
    default:
        return (0); // otherwise: return zero, if character has to be ignored
    }
}

inline String
utf8ascii(const String &s) {
    String r = "";
    char   c;
    for(auto i = 0u; i < s.length(); ++i) {
        c = utf8ascii(s.charAt(i));
        if(c != 0) r += c;
    }
    return r;
}

} // namespace Util

#endif // UTILITIES_H
