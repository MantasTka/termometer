#ifndef PROJECT_STATE_H
#define PROJECT_STATE_H

class LiquidCrystal;
class DHT11;

class State{
public:
    virtual ~State()=default;
    void onExit(LiquidCrystal & lcd); //& - perduodi reference (kad nekopijuotu objektu perduodu reference)
    virtual void onEnter(LiquidCrystal & lcd)=0;
    virtual void update(LiquidCrystal & lcd, DHT11 & dht11)=0;
    virtual State* handleInput(bool input)=0;
};







#endif