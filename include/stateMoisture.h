#ifndef PROJECT_STATEMOISTURE_H
#define PROJECT_STATEMOISTURE_H

#include "state.h"
#include "Arduino.h"

class StateMoisture:public State{
public:
    StateMoisture();
    virtual ~StateMoisture()=default;
    void onEnter(LiquidCrystal & lcd) override;
    void update(LiquidCrystal & lcd, DHT11 & dht11) override;
    State* handleInput(bool input) override;
private:
    const String msg;
    int moisture;
protected:
    const String & getMsg();
    String getMoistureMsg(DHT11 & dht11);
};

#endif