
// #ifndef PROJEKTOPAVADINIMAS_FAILOPAVADINIMAS #ifndef - if not defined  #define - definino

#ifndef PROJECT_THERMOMETER_H
#define PROJECT_THERMOMETER_H

#include <EduIntro.h>
#include <LiquidCrystal.h>

#define DHTPIN D7
#define RS 12
#define EN 11
#define D_PIN4 5
#define D_PIN5 4
#define D_PIN6 3
#define D_PIN7 8

#define COLS 16
#define ROWS 2

class State;

class Thermometer
{
private:
    State *thermometerState;
    LiquidCrystal lcd;
    DHT11 dht11;
 
public:
    void setState(State* state);
    void update();
    void handleInput(bool input);
    Thermometer();
    ~Thermometer();
};

#endif