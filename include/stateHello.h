#ifndef PROJECT_STATEHELLO_H
#define PROJECT_STATEHELLO_H

#include "state.h"

#define CURSOR_LOCATION 7

class StateHello:public State{
public:
    virtual ~StateHello()=default;
    void onEnter(LiquidCrystal & lcd) override;
    void update(LiquidCrystal & lcd, DHT11 & dht11) override;
    State* handleInput(bool input) override;
};






#endif