#ifndef PROJECT_STATEBOTH_H
#define PROJECT_STATEBOTH_H

#include "state.h"
#include "stateTemp.h"
#include "stateMoisture.h"

class StateBoth:public StateTemp, public StateMoisture{
public:
    virtual ~StateBoth()=default;
    void onEnter(LiquidCrystal & lcd) override;
    void update(LiquidCrystal & lcd, DHT11 & dht11) override;
    State* handleInput(bool input) override;
};


#endif